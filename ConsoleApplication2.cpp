﻿#include <iostream>


class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Animal voice"<<'\n';
	}
};

class Dog : public Animal
{
public:
	void Voice()
	{
		std::cout << "Woof" << '\n';
	}
};

class Cat : public Animal
{
public:
	void Voice()
	{
		std::cout << "Meow" << '\n';
	}
};

class Cow : public Animal
{
public:
	void Voice()
	{
		std::cout << "Moo" << '\n';
	}
};



int main()
{
	const int animalCount = 3;
	Animal* animals[animalCount];

	animals[0] = new Dog();
	animals[1] = new Cat();
	animals[2] = new Cow();

	for (int i = 0; i < animalCount; i++) {
		animals[i]->Voice();
	}
}